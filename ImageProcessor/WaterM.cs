﻿using System;
using System.Drawing;

namespace ImageProcessor
{
    public static class WaterM
    {
        //This method makes a workable copy of an image then places a  water mark on it
        //and saves it to the destenation folder.
        public static void Marker(string markMe, string dPath)
        {
            Image bitmap =  Bitmap.FromFile(markMe);
            Graphics graphicsImage = Graphics.FromImage(bitmap);
            StringFormat stringformat1 = new StringFormat();
            stringformat1.Alignment = StringAlignment.Near;
            Color StringColor = ColorTranslator.FromHtml("#999999");
            string Str_TextOnImage = "New Moon Adventures";
            graphicsImage.DrawString(Str_TextOnImage, new Font("arial", 20, FontStyle.Regular),
            new SolidBrush(StringColor), new Point(0, 0), stringformat1);
            Console.WriteLine(dPath);
            bitmap.Save(dPath);
            bitmap.Dispose();
        }
    }
}